import numpy as np
from gr_pyhole.scene import Scene

scene = Scene()         # create new scene and set values
scene.metric = 'Schwarzschild'
scene.r = 15.0
scene.theta = np.deg2rad(90.0)
scene.size = (1024,1024)
scene.tolerance = scene.LOW_ACCURACY

if not scene.load():    # try to load from auto-generated
    scene.raytrace()    # file name, otherwise raytrace

scene.saveImage()
scene.saveEHImage('ex2.png')
scene.saveDescription()

scene.show()            # start interactive display
