import numpy as np
from gr_pyhole.scene import Scene

scene = Scene()         # create new scene and set values
scene.metric = 'Schwarzschild'
scene.r = 15.0
scene.theta = np.deg2rad(90.0)
scene.size = (2048,1024)        # 2:1 aspect ratio
scene.fov = (np.pi, np.pi/2.0)  # field of view
scene.projection = 'Equirectangular'
scene.tolerance = scene.MEDIUM_ACCURACY

if not scene.load():
    scene.raytrace_parallel(__name__)

scene.saveWebGLTexture('schwarzschild-texture.png')
