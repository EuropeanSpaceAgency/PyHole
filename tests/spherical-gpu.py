# -*- coding: utf-8 -*-
"""
Created on Wed Oct 14 19:28:34 2015

A simple code to view precomputed data files and analyze them.

@author: Alexander Wittig
"""

import unittest, sys, os
# only needed to set up the relative path to local pyhole module if not installed system-wide
# sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

import numpy as np
from gr_pyhole import metric, observer, propagator
from gr_pyhole.image import Image

class GPUTestSphericalMetricsEquirectangular(unittest.TestCase):
    TOLERANCE = 1e-6
    #REAL = np.float32
    REAL = np.float64
    PLATFORM_ID = 1
    DEVICE = "CPU"
    SIZE = (256,256)
    RSKY = 30.0
    DATAFILE = 'configuration-2.npz'
    OBS = observer.Equirectangular(r=15.0, theta=np.pi/2)
    PREFIX = "GPU-SPH-EQU-"

    def getPropagator(self, g):
        p = propagator.SphericalGPU(self.OBS, g, Rsky=self.RSKY, device=self.DEVICE)
        p.TOLERANCE = self.TOLERANCE
        p.real = self.REAL
        p.PLATFORM_ID = self.PLATFORM_ID
        p.VERBOSE = False   # don't print extra output
        return p

    def runtest(self, g, name):
        p = self.getPropagator(g)
        i = Image(p, self.SIZE)
        i.saveImage(self.PREFIX+name+'.png')
        return True        # no way to test if the image looks right

    def test_Flat(self):
        g = metric.Flat()
        assert(self.runtest(g, 'flat'))

    def test_CFlat(self):
        g = metric.CFlat()
        self.assertRaises(ValueError, self.runtest, g, 'cflat')      # spherical propagator with cartesian metric

    def test_Schwarzschild(self):
        g = metric.Schwarzschild(2.0)
        assert(self.runtest(g, 'schwarzschild'))

    def test_Kerr(self):
        g = metric.Kerr(2.0, 1.0)
        self.assertRaises(ValueError, self.runtest, g, 'kerr')      # Kerr doesn't have a GPU implementation yet

    def test_HR_Flat(self):
        g = metric.HR(metric.hr.Flat())
        assert(self.runtest(g, 'hr-flat'))

    def test_HR_Schwarzschild(self):
        g = metric.HR(metric.hr.Schwarzschild(2.0))
        assert(self.runtest(g, 'hr-schwarzschild'))

    def test_HR_Interpolated(self):
        g = metric.HR(metric.hr.Interpolated(self.DATAFILE))
        assert(self.runtest(g, 'hr-interpolated'))

class GPUTestSphericalMetricsStereographic(GPUTestSphericalMetricsEquirectangular):
    OBS = observer.Stereographic(r=15.0, theta=np.pi/2)
    PREFIX = "GPU-SPH-STR-"

class GPUTestSphericalMetricsGnomonic(GPUTestSphericalMetricsEquirectangular):
    OBS = observer.Gnomonic(r=15.0, theta=np.pi/2)
    PREFIX = "GPU-SPH-GNO-"

class GPUTestSphericalMetricsEquirectangularPole(GPUTestSphericalMetricsEquirectangular):
    OBS = observer.Equirectangular(r=15.0, theta=0.01)  # note that theta=0 does not work
    PREFIX = "GPU-SPH-EQU-POLE-"

if __name__ == "__main__":
    unittest.main()
