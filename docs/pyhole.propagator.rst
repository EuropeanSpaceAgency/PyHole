gr_pyhole.propagator package
============================

Submodules
----------

gr_pyhole.propagator.cpu module
-------------------------------

.. automodule:: gr_pyhole.propagator.cpu
    :members:
    :undoc-members:
    :show-inheritance:

gr_pyhole.propagator.gpu module
-------------------------------

.. automodule:: gr_pyhole.propagator.gpu
    :members:
    :undoc-members:
    :show-inheritance:

gr_pyhole.propagator.propagator module
--------------------------------------

.. automodule:: gr_pyhole.propagator.propagator
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------
The gr_pyhole.propagator module itself contains all propagator objects as defined in each of its submodules.
This allows simple statements such as ``from gr_pyhole.propagator import SphericalCPU`` instead of 
the more complicated ``from gr_pyhole.propagator.cpu import SphericalCPU``.

See the documentation of each submodule above for details on each.
