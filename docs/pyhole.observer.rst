gr_pyhole.observer package
==========================

Submodules
----------

gr_pyhole.observer.equirectangular module
-----------------------------------------

.. automodule:: gr_pyhole.observer.equirectangular
    :members:
    :undoc-members:
    :show-inheritance:

gr_pyhole.observer.gnomonic module
----------------------------------

.. automodule:: gr_pyhole.observer.gnomonic
    :members:
    :undoc-members:
    :show-inheritance:

gr_pyhole.observer.observer module
----------------------------------

.. automodule:: gr_pyhole.observer.observer
    :members:
    :undoc-members:
    :show-inheritance:

gr_pyhole.observer.stereographic module
---------------------------------------

.. automodule:: gr_pyhole.observer.stereographic
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------
The gr_pyhole.observer module itself contains all observer objects defined in each of its submodules.
This allows simple statements such as ``from gr_pyhole.observer import Equirectangular`` instead of 
the more complicated ``from gr_pyhole.observer.equirectangular import Equirectangular``.

See the documentation of each submodule above for details on each.
