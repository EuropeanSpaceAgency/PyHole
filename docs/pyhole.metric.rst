gr_pyhole.metric package
========================

Submodules
----------

gr_pyhole.metric.base module
----------------------------

.. automodule:: gr_pyhole.metric.base
    :members:
    :undoc-members:
    :show-inheritance:

gr_pyhole.metric.flat module
----------------------------

.. automodule:: gr_pyhole.metric.flat
    :members:
    :undoc-members:
    :show-inheritance:

gr_pyhole.metric.hr module
--------------------------

.. automodule:: gr_pyhole.metric.hr
    :members:
    :undoc-members:
    :show-inheritance:

gr_pyhole.metric.kerr module
----------------------------

.. automodule:: gr_pyhole.metric.kerr
    :members:
    :undoc-members:
    :show-inheritance:

gr_pyhole.metric.schwarzschild module
-------------------------------------

.. automodule:: gr_pyhole.metric.schwarzschild
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------
The gr_pyhole.metric module itself contains all metric objects defined in each of its submodules.
This allows simple statements such as ``from gr_pyhole.metric import Flat`` instead of 
the more complicated ``from gr_pyhole.metric.flat import Flat``.

See the documentation of each submodule above for details on each.
